const { Router } = require("express");
const router = Router();
const ProductController = require("../controllers/ProductController");
const UserController = require("../controllers/UserController");

//rotas dos produtos
router.get('/products', ProductController.index);
router.get('/products/:id',ProductController.show);
router.post('/products', ProductController.create);
router.put('/products/:id',ProductController.update);
router.delete('/products/:id',ProductController.destroy);
router.put('/products/:productId/users/:userId', ProductController.purchase);
router.put('/products/cancelPurchase/:id', ProductController.cancelPurchase)

//rotas dos usuários
router.get('/users', UserController.index);
router.get('/users/:id',UserController.show);
router.post('/users', UserController.create);
router.put('/users/:id',UserController.update);
router.delete('/users/:id',UserController.destroy);

module.exports = router;