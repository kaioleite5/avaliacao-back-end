
const Product = require('../models/Product');
const User = require('../models/User');

//Mostrar todos os produtos
index = async(req,res) =>{
    try{
        const products = await Product.findAll();
        return res.status(200).json({products});
    }
    catch(err){
        return res.status(500).json({err});
    }
}

//Mostrar um produto específico
show = async (req,res) => {
    try{
        const {id} = req.params;
        const product = await Product.findByPk(id)
        return res.status(200).json({product});
    }
    catch(err){
        return res.status(500).json({err});
    }
}

//Criar um produto
create = async (req,res) =>{
    try{
        const product = await Product.create(req.body);
        console.log(req.body);
        return res.status(200).json({message:'Produto foi cadastrado com sucesso.', product:product});
    }
    catch(err){
        return res.status(500).json({err});
    }
}

//Editar um produto
update = async (req,res) =>{
    try{
        const {id} = req.params; 
        const updated = await Product.update(req.body,{where:{id:id}})
        if (updated){
            const product = await Product.findByPk(id);
            return res.status(200).send(product);
        }
        throw new Error ();

    }
    catch(err){
        return res.status(500).json('Produto não encontrado.');
    }
}

//Deletar um produto
destroy = async (req,res) =>{
    try{
        const {id} = req.params;
        const deleted = await Product.destroy({where:{id:id}})
        if (deleted){
            return res.status(200).json('Produto deletado com sucesso.');
        }
        throw new Error ();
    }
    catch(err){
        return res.status(500).json('Produto não encontrado.');
    }
}

//Estabelecer a relação de compra com o usuário
purchase = async(req,res)=>{
    const {productId,userId} = req.params;
    try{
        const product = await Product.findByPk(productId);
        const user = await User.findByPk(userId);
        if (product.stock>0){
            await product.setUser(user);
            await Product.update({stock:0},{where:{id:productId}})
            return res.status(200).json('Compra realizada com sucesso.');
        }else{
            return res.status(200).json('Produto sem estoque.')
        }
    }
    catch(err){
        return res.status(500).json('Ocorreu um problema na compra')
    }
}

//Remover a relação de compra com o usuário
cancelPurchase = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        if (product.UserId==null){
            return res.status(200).json('Produto não foi comprado.')
        }else{
            await Product.update({stock:1},{where:{id:id}})
            await product.setUser(null);
            return res.status(200).json("Compra cancelada.");
        }
    }catch(err){
        return res.status(500).json('Ocorreu um erro');
    }
}

module.exports = {
    update,
    destroy,
    create,
    index,
    show,
    purchase,
    cancelPurchase
}