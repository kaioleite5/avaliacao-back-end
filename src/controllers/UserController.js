
const User = require ('../models/User');

//Mostrar todos os usuários
index = async(req,res) =>{
    try{
        const users = await User.findAll();
        return res.status(200).json({users});
    }
    catch(err){
        return res.status(500).json({err});
    }
}

//Mostrar um usuário específico
show = async (req,res) => {
    try{
        const {id} = req.params;
        const user = await User.findByPk(id)
        return res.status(200).json({user});
    }
    catch(err){
        return res.status(500).json({err});
    }
}

//Criar um usuário
create = async (req,res) =>{
    try{
        const user = await User.create(req.body);
        console.log(req.body);
        return res.status(200).json({message:'Usuário cadastrado com sucesso.', user:user});
    }
    catch(err){
        return res.status(500).json({err});
    }
}

//Editar um usuário
update = async (req,res) =>{
    try{
        const {id} = req.params; 
        const updated = await User.update(req.body,{where:{id:id}})
        if (updated){
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        }
        throw new Error ();

    }
    catch(err){
        return res.status(500).json('Usuário não encontrado.');
    }
}

//Deletar um usuário
destroy = async (req,res) =>{
    try{
        const {id} = req.params;
        const deleted = await User.destroy({where:{id:id}})
        if (deleted){
            return res.status(200).json('Usuário deletado com sucesso.');
        }
        throw new Error ();
    }
    catch(err){
        return res.status(500).json('Usuário não encontrado.');
    }
}

module.exports = {
    update,
    destroy,
    create,
    index,
    show
}
