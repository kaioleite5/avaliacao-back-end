const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User',{
    name:{
        type:DataTypes.STRING,
        allowNull:false
    },
    address:{
        type:DataTypes.STRING,
        allowNull: false
    },
    email:{
        type: DataTypes.STRING,
        allowNull: false
    },
    phone:{
        type: DataTypes.STRING,
        allowNull: false
    },
    birthday: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    cpf: {
        type: DataTypes.STRING,
        allowNull: false
    },
    userImg: {
        type: DataTypes.STRING
    }
})

User.associate = function(models){
    User.hasMany(models.Product)
}

module.exports = User;