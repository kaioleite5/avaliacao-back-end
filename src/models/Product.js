const { database } = require("faker-br/lib/locales/en");
const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Product = sequelize.define('Product',{
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price:{
        type:DataTypes.DOUBLE,
        allowNull: false
    },
    description:{
        type: DataTypes.STRING
    },
    category:{
        type: DataTypes.STRING,
        allowNull: false
    },
    stock: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    productImg:{
        type: DataTypes.STRING,
        allowNull: false
    }
})

Product.associate = function(models){
    Product.belongsTo(models.User)
};

module.exports = Product;