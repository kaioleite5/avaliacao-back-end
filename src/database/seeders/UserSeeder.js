const User = require("../../models/User");
const faker = require('faker-br');

//Gerar 10 usuários aleatórios
const seedUser = async function () {
  try {
    await User.sync({ force: true });

    for (let i = 0; i < 10; i++) {
      await User.create({
        name: faker.name.firstName(),
        address: faker.address.streetAddress(),
        email: faker.internet.email(),
        phone: faker.phone.phoneNumber(),
        birthday: faker.date.between(1990, 2004),
        cpf: faker.br.cpf(),
        userImg: 'http://lorempixel.com.br/640/480/'
      });
    }
  } catch (err) { 
    console.log(err); 
  }
};

module.exports = seedUser;
