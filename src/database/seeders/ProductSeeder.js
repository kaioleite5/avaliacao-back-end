const Product = require("../../models/Product");
const faker = require('faker-br');

//Gerar 10 produtos aleatórios
const seedProduct = async function () {
    try {
        await Product.sync({ force: true });

        for (let i = 0; i < 10; i++) {
            await Product.create({
                title: faker.commerce.productName(),
                price: faker.commerce.price(30, 800, 2),
                description: faker.lorem.text(),
                category: faker.commerce.department(),
                stock: 1,
                productImg: 'http://lorempixel.com.br/640/480/'
            });
        }
    } catch (err) {
        console.log(err);
    }
};

module.exports = seedProduct;